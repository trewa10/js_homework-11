"use strict";

const passForm = document.querySelector(".password-form");
const passOrigin = document.querySelector("#pass-origin");
const passConfirm = document.querySelector("#pass-confirm");
const allertDiv = document.querySelector(".allert");

passForm.addEventListener("click", function(event) {
    if (event.target.closest(".icon-password")) {
        passShower(event.target);
    } else if (event.target.closest(".btn")) {
        passChecker();
        event.preventDefault();
        // також тут можна викликати функцію, яка відправлятиме форму замість дефолтної відправки, яку ми заборонили
    }
})

function passShower (elem) {
    if (elem.classList.contains("fa-eye")) {
        elem.classList.remove("fa-eye");
        elem.classList.add("fa-eye-slash");
        elem.previousElementSibling.setAttribute("type", "text");
    } else {
        elem.classList.remove("fa-eye-slash");
        elem.classList.add("fa-eye");
        elem.previousElementSibling.setAttribute("type", "password");
    }
}

function passChecker () {
    if (passOrigin.value && passConfirm.value && (passConfirm.value === passOrigin.value)) {
    allertDiv.classList.add("hidden");
    setTimeout(() => alert("You are welcome!"), 0);
    // додав нульовий таймер щоб алерт виконувався після того, як зникне повідомлення про помилку введення паролів 
    } else {
        allertDiv.classList.remove("hidden");
    }

}

